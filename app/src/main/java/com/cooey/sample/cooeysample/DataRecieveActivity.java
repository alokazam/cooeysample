package com.cooey.sample.cooeysample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.biz.cooey.BPData;
import com.biz.cooey.CooeyBleDeviceFactory;
import com.biz.cooey.CooeyBleDeviceManager;
import com.biz.cooey.CooeyDeviceDataReceiveCallback;
import com.biz.cooey.CooeyDeviceType;
import com.biz.cooey.CooeyProfile;
import com.biz.cooey.WeightData;
import com.lifesense.ble.bean.LsDeviceInfo;

public class DataRecieveActivity extends AppCompatActivity {

    private CooeyBleDeviceManager mCooeyBleManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_recieve);
        if(DataStore.pairedDevices.size() == 0){
            Toast.makeText(this, "There are no device that are paired.", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(this, "Started recieving data.", Toast.LENGTH_LONG).show();
            startDataReceiveService();
        }
    }

    private void startDataReceiveService() {
        mCooeyBleManager = getBleManager();
        mCooeyBleManager.deRegisterAllDevices();
        for(LsDeviceInfo lsDeviceInfo : DataStore.pairedDevices) {
            mCooeyBleManager.registerPairedDevice(lsDeviceInfo);
        }
        mCooeyBleManager.receiveData(new CooeyDeviceDataReceiveCallback() {

            @Override
            public void onReceiveWeightdata(final WeightData wd) {
                Toast.makeText(DataRecieveActivity.this, "The Weight received is " + wd.getWeightKg(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onReceiveBPData(final BPData bpd) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_data_recieve, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public CooeyBleDeviceManager getBleManager(){
        if (mCooeyBleManager != null)
            return mCooeyBleManager;
        CooeyBleDeviceFactory mFactory = new CooeyBleDeviceFactory(
                new int[]{CooeyDeviceType.BP_METER,
                        CooeyDeviceType.WEIGHT_AND_FAT_SCALE});
        mCooeyBleManager = (CooeyBleDeviceManager) mFactory
                .getBleDeviceManager();
        //Replace with the Cooey Profile
        CooeyProfile cooeyProfile = new CooeyProfile();
        cooeyProfile.setFirstName("Alok");
        cooeyProfile.setLastName("Monty");
        cooeyProfile.setEmail("alokazam@hotmail.com");
        cooeyProfile.setImageUrl("https://thedeadbird.files.wordpress.com/2008/11/whatever-bottom1.jpg");
        cooeyProfile.setDob("1989-20-02");
        cooeyProfile.setGender("male");
        cooeyProfile.setHeight(126);
        cooeyProfile.setAthleteLevel(5);
        cooeyProfile.setIsloggedin(1);
        cooeyProfile.setMobileNumber("7893416930");
        cooeyProfile.setPatientId(1000);
        cooeyProfile.setWeight(70.8f);
        mCooeyBleManager.initialize(new CooeyProfile(), this);

        return mCooeyBleManager;
    }
}
