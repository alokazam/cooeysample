package com.cooey.sample.cooeysample;

import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.biz.cooey.CooeyBleDeviceFactory;
import com.biz.cooey.CooeyBleDeviceManager;
import com.biz.cooey.CooeyDevicePairCallback;
import com.biz.cooey.CooeyDeviceSearchCallback;
import com.biz.cooey.CooeyDeviceType;
import com.biz.cooey.CooeyProfile;
import com.lifesense.ble.bean.DeviceUserInfo;
import com.lifesense.ble.bean.LsDeviceInfo;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    CooeyBleDeviceManager mCooeyBleManager;
    DeviceListAdapter deviceListAdapter = new DeviceListAdapter();
    UserProfileDialogHandler userProfileDialogHandler = new UserProfileDialogHandler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView devicesList = (ListView) findViewById(R.id.devices_list);
        devicesList.setAdapter(deviceListAdapter);
        devicesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mCooeyBleManager.pairDevice(DataStore.devices.get(position), new pairCallback(position));
            }
        });
        initializeBluetooth();
        initializeCooeyDeviceManager();
        searchDevices();
    }

    private void searchDevices() {
        DataStore.devices.clear();
        mCooeyBleManager.searchDevicesForPairing(new CooeyDeviceSearchCallback() {

            @Override
            public void onDeviceFound(final LsDeviceInfo arg0) {
                // TODO Auto-generated method stub
                if(!DataStore.devices.contains(arg0)){
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            DataStore.devices.add(arg0);
                            deviceListAdapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });
    }


    private void initializeCooeyDeviceManager() {
        mCooeyBleManager = getBleManager();
    }

    private void initializeBluetooth() {
        try {
            BluetoothAdapter bluetoothAdapter = BluetoothAdapter
                    .getDefaultAdapter();
            bluetoothAdapter.enable();
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, DataRecieveActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public CooeyBleDeviceManager getBleManager(){
        if (mCooeyBleManager != null)
            return mCooeyBleManager;
        CooeyBleDeviceFactory mFactory = new CooeyBleDeviceFactory(
                new int[]{CooeyDeviceType.BP_METER,
                        CooeyDeviceType.WEIGHT_AND_FAT_SCALE, CooeyDeviceType.WEIGHT_SCALE, CooeyDeviceType.FAT_SCALE });
        mCooeyBleManager = (CooeyBleDeviceManager) mFactory
                .getBleDeviceManager();
        //Replace with the Cooey Profile
        CooeyProfile cooeyProfile = new CooeyProfile();
        cooeyProfile.setFirstName("Alok");
        cooeyProfile.setLastName("Monty");
        cooeyProfile.setEmail("alokazam@hotmail.com");
        cooeyProfile.setImageUrl("https://thedeadbird.files.wordpress.com/2008/11/whatever-bottom1.jpg");
        cooeyProfile.setDob("1989-20-02");
        cooeyProfile.setGender("male");
        cooeyProfile.setHeight(126);
        cooeyProfile.setAthleteLevel(5);
        cooeyProfile.setIsloggedin(1);
        cooeyProfile.setMobileNumber("7893416930");
        cooeyProfile.setPatientId(1000);
        cooeyProfile.setWeight(70.8f);
        mCooeyBleManager.initialize(new CooeyProfile(), this);

        return mCooeyBleManager;
    }


    class DeviceListAdapter extends BaseAdapter{

        public DeviceListAdapter() {
            super();
        }

        @Override
        public int getCount() {
            return DataStore.devices.size();
        }

        @Override
        public Object getItem(int position) {
            return DataStore.devices.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
            convertView = layoutInflater.inflate(R.layout.layout_list_item, parent, false);
            TextView textView = (TextView) convertView.findViewById(R.id.deviceName);
            textView.setText(DataStore.devices.get(position).getDeviceName());
            return convertView;
        }
    }


    //Initializing Pair Callback
    private class pairCallback extends CooeyDevicePairCallback {

        private int pos = -1;

        public pairCallback(int _pos) {
            pos = _pos;
        }

        @Override
        public void onPairingResult(final LsDeviceInfo arg0, final int status) {
            if (status == -1) {
                Toast.makeText(MainActivity.this, "Pairing failed, please try again", Toast.LENGTH_SHORT).show();
                return;
            }
            MainActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "Device paired", Toast.LENGTH_SHORT).show();
                    DataStore.pairedDevices.add(arg0);
                }
            });
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onDiscoverUserInfo(final List arg0) {
            userProfileDialogHandler.setDeviceUserInfo(arg0);
            MainActivity.this.runOnUiThread(userProfileDialogHandler);
        }

    }


    private class UserProfileDialogHandler implements Runnable {
        private List<DeviceUserInfo> curDevInfo = null;
        private Boolean isRunning = false;
        public Boolean wasCancelled = null;

        public UserProfileDialogHandler() {
            //curDevInfo = new ArrayList<DeviceUserInfo>(dui); //copy, dont refer

        }

        public void setDeviceUserInfo(List<DeviceUserInfo> dui) {
            if (isRunning) return;
            curDevInfo = new ArrayList<DeviceUserInfo>(dui);
        }

        @Override
        public void run() {

            if (isRunning) return;
            isRunning = true;
            final ArrayList<String> items = new ArrayList<String>();

            for (int i = 0; i < curDevInfo.size(); ++i) {
                items.add("Profile Button " + (i + 1));//((DeviceUserInfo)curDevInfo.get(i)).getUserName());
            }

            AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);

            builder2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    dialog.dismiss();
                    int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                    String s = items.get(selectedPosition);
                    mCooeyBleManager.bindUserProfileWithName(selectedPosition + 1, "Alok");
                    isRunning = false;
                    wasCancelled = false;
                }
            })
                    .setTitle("Choose profile button for '" + "Alok" + "'")
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //mManager.bindUserProfileWithName(0 , activeProfile.getFirstName());
                            wasCancelled = true;
                            dialog.dismiss();
                        }
                    }).setSingleChoiceItems(items.toArray(new String[items.size()]), 0, null)
                    .create()
                    .show();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        mCooeyBleManager.stopSearchForDevices();
    }



}
